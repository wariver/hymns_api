<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DBPoliceMan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'go:police';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        echo $this->reset_table_vals();
        return null;
    }


    private function reset_table_vals()
    {
        $time_now = Carbon::now('Africa/Nairobi')->toDateTimeString();
        $tbl_hymns_id_seq = DB::select(DB::raw("SELECT setval('tbl_hymns_id_seq',(SELECT GREATEST(MAX(id)+1,nextval('tbl_hymns_id_seq'))-1 FROM tbl_hymns))"));
        $tbl_mobile_users_seq = DB::select(DB::raw("SELECT setval('tbl_mobile_users_id_seq',(SELECT GREATEST(MAX(id)+1,nextval('tbl_mobile_users_id_seq'))-1 FROM tbl_mobile_users))"));

        $table_refresh_log =
            'hymns table id refreshed: ' . json_encode($tbl_hymns_id_seq) . ' -> ' . $time_now . ' \n.'
            . 'mobile users table id refreshed: ' . json_encode($tbl_mobile_users_seq) . ' -> ' . $time_now . '\n';
        Storage::append(storage_path('file.txt'), $table_refresh_log);
        return response()->json(['status' => 200, 'message' => 'refresh complete']);
    }
}
