<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class UserTokens
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token_owner = $request->user()['id'];
        if ($request->method() === 'GET') {
            $user_id = (int)basename($request->path());
        } else {
            $user_id = (int)$request->get('user_id');
        }
        if ($token_owner !== $user_id) {
//            dd($token_owner, $user_id, $request->url(), $request->all());
            return response()->json(['status' => 501, 'error' => 'Not Allowed'], 500);
        }

        return $next($request);
    }
}
