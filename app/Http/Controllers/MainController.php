<?php

namespace App\Http\Controllers;

use App\Hymns;
use App\HymnsUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Lcobucci\JWT\Parser;

class MainController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $ranString;
    public $user_agent;

    public function __construct()
    {
    }

    protected $redirectTo = '/dashboard';

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $request->validate([
            'fullname' => 'required|string',
            'email' => 'required|string|email|unique:tbl_users',
            'phone' => 'required|max:13|unique:tbl_users',
            'actype' => 'required|integer',
            'password' => 'required|string|confirmed'
        ]);
        $api = new Api();
        $phone = json_decode($api->phoneFormat($request->phone), true);
        if ($phone['status'] === true) {
            $phone_number = $phone['formatedPhone'];
        } else {
            return response()->json(['status' => 501, 'error' => 'The format of the phone given could not be processed.']);
        }

        $user = new HymnsUser([
            'fullname' => $request->fullname,
            'email' => $request->email,
            'phone' => $phone_number,
            'actype' => (int)$request->actype,
            'acstatus' => 0,
            'password' => bcrypt($request->password)
        ]);

        $user->save();

        return response()->json([
            'status' => 201,
            'message' => 'Registration was successful. Please wait for account activation.',
            'redirect' => 'auth/login'
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $email_number = $request->email;
        if (is_numeric($email_number)) {
            $api = new Api();
            $phone_number = 0;
            $phone = json_decode($api->phoneFormat($email_number), true);
            if ($phone['status'] === true) {
                $phone_number = $phone['formatedPhone'];
            } else {
                return response()->json(['status' => 501, 'error' => 'The format of the phone given could not be processed.']);
            }
            $val = DB::table('tbl_users')->where('phone', $phone_number)->first();
            if ($val === null) {
                return response()->json(['status' => 501, 'error' => 'Could not find user.']);
            }
            if (strlen($request->password) === 4) {
                $user_data = DB::table('tbl_users')->where('phone', $phone_number)->first();

                if (!$user_data) {
                    return response()->json(['status' => 501, 'error' => 'Could not find user.']);
                }

                $user_id = $user_data->id;

                $stored_pin = DB::table('tbl_payments_password')->select(['pin'])->where('user_id', $user_id)->first();

                if (!isset($stored_pin->pin)) {
                    return response()->json(['status' => 501, 'error' => 'Please create a pin first.']);
                }
                $decrypted = Crypt::decryptString($stored_pin->pin);

                if ($request->password === $decrypted) {
                    $loginFrmPhone = true;
                    Auth::loginUsingId($user_id);
//                    return response()->json(['status' => 200, 'msg' => 'Correct pin proceed.']);
                }
            }
            $request['email'] = $val->email;
        }
        $userCredentials = request(['email', 'password']);
        $userCredentials['acstatus'] = 1;
//        GET USER PIN NUMBER

        if (!Auth::attempt($userCredentials)) {
            return response()->json(['error' => 'Check email and password']);
        }
        $user = $request->user();

        $tokenResult = $user->createToken('User Personal Access Token');
        $token = $tokenResult->token;

        $loginAccess = array(
            'msg' => 'Login Successful',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'redirect' => 'dashboard');

        return response()->json($loginAccess);

    }

    public function logout(Request $request)
    {
        $value = $request->bearerToken();
        if ($value) {
            $id = (new Parser())->parse($value)->getHeader('jti');
            $token = $request->user()->tokens->find($id);
            $token->revoke();
        }
        return response(['code' => 200, 'message' => 'You have been successfully logged out']);
    }

    //validate phone
    function validatePhone($phone)
    {
        $return = 0;
        /*if(substr($phone, 0,2) === "07")//if phone starts with 07
        {
            $phone = substr_replace($phone, "254", 0,1);
        }
        elseif(substr($phone, 0,1) === "7")//if starts with 7
        {
            $phone = substr_replace($phone, "254", 0,0);
        }
        elseif(substr($phone, 0,4) === "+254")
        {
            $phone = substr_replace($phone, "", 0,1);
        }*/
        if (strlen($phone) >= 10 && is_numeric($phone))//phone correct
        {
            $return = 1;
        }

        return $return;
    }


    function generateRandomString($length = 10)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            try {
                $randomString .= $characters[random_int(0, $charactersLength - 1)];
            } catch (\Exception $e) {
            }
        }
        return $randomString;
    }

    function generateRandomHymnNumber($length = 4)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            try {
                $randomString .= $characters[random_int(0, $charactersLength - 1)];
            } catch (\Exception $e) {
            }
        }
        return $randomString;
    }

    protected function validator(array $data)
    {
        $phone = $data['phone'];
        $email = $data['email'];

        date_default_timezone_set('Africa/Nairobi');
        $now = date('Y-m-d H:i:s');

        //format phone
        if (strpos($phone, '07') === 0)//if phone starts with 07
        {
            $phone = substr_replace($phone, '254', 0, 1);
        } elseif (strpos($phone, '7') === 0)//if starts with 7
        {
            $phone = substr_replace($phone, '254', 0, 0);
        } elseif (strpos($phone, '+254') === 0) {
            $phone = substr_replace($phone, '', 0, 1);
        }
        if ($this->validatePhone($phone) === 1) {
            $response['phone'] = $phone;
        } else {
            return response(['status' => 501, 'error' => 'phone number is invalid']);
        }
        if (!$query = DB::table('tbl_users')->where('email', $email)->get()) {
            $response = array('status' => 501, 'error' => 'Failed to fetch email');
        } else {
            if (count($query) > 0)//if email is in use
            {
                return response(['status' => 501, 'error' => 'This email is already registered']);
            }
        }

        return Validator::make($data, [
            'fullname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:tbl_users',
            'password' => 'required|string|min:4|confirmed'
        ]);
    }

    public function profile(Request $request)
    {
        return response()->json($request->user());
    }

    public function user_hymns(Request $request)
    {
        $language = $request->query('lang');

        $hymns = Hymns::where('language', $language)->orderBy('number', 'asc')->get();
        return response()->json(['data' => $hymns, 'count' => count($hymns)]);
    }

    public function search_hymns(Request $request)
    {
        $search_key_title = $request->get('s_k_title');
        $search_key_text = $request->get('s_k_text');

        return Hymns::where('text', 'ILIKE', '%' . $search_key_text . "%")->limit(20)->get();
//        return Hymns::where('title', 'ILIKE', '%' . $search_key_title . "%")->orWhere('text', 'LIKE', '%' . $search_key_text . "%")->limit(10)->get();
    }

    public function add_hymn(Request $request)
    {
        $hymn_title = $request->get('title');
        $user_id = $request->get('us_id');
        $hymn_language = $request->get('language');
        $hymn_key = $request->get('key');
        $hymn_text = $request->get('text');

        $insert_array = [
            'title' => $hymn_title,
            'text' => $hymn_text,
            'user_id' => $user_id,
            'number' => $this->generateRandomHymnNumber(),
            'key' => $hymn_key,
            'language' => $hymn_language,
            'created_at' => Carbon::now(),
            'approved' => 0
        ];

        if ($insert_tentative = DB::table('tbl_hymmns_tentative')->insert($insert_array)) {
            return response()->json(['status' => 200, 'msg' => 'Hymn Submited for approval.']);

        }
        return response()->json(['status' => 501, 'error' => 'could not submit hymn for approval']);

    }

    public function update_hymn(Request $request)
    {
        $hymn_id = $request->get('h_id');
        $hymn_title = $request->get('title');
        $hymn_number = $request->get('number');
        $hymn_language = $request->get('language');
        $hymn_key = $request->get('key');
        $hymn_text = $request->get('text');

        $update_array = [
            'title' => $hymn_title,
            'text' => $hymn_text,
            'number' => $hymn_number,
            'key' => $hymn_key,
            'language' => $hymn_language,
            'updated_at' => Carbon::now()
        ];

        if ($insert_tentative = DB::table('tbl_hymmns')->where('id', $hymn_id)->update($update_array)) {
            return response()->json(['status' => 200, 'msg' => 'Hymn updated.']);

        }
        return response()->json(['status' => 501, 'error' => 'could not update hymn']);

    }

    public function fetch_new_hymns(Request $request)
    {
//        $phone_number = $request->get('phone_number');
//        $pin = $request->get('pin');
        $last_sync = $request->get('last_sync');

        /*$date_sync1 = date("D M j G:i:s T Y", strtotime($last_sync));
        $date_sync2 = date("Y-m-d H:i:s", strtotime($last_sync));
        echo $date_sync1. '|';
        echo $date_sync2;
        return;*/
//        $user = DB::table('tbl_mobile_users')->where('phone', $phone_number)->where('password', $pin)->first();
//        if (!isset($phone_number, $pin, $last_sync) || !$user) {
//            return response()->json(['status' => 501, 'error' => 'You are not authorised to access this resource.']);
//        }
        return response()->json(['status' => 200, 'data' => DB::table('tbl_hymns')->where('updated_at', '>', $last_sync)->get()]);
    }


    /*public function fetch_old_hymns()
    {
        $client = new Client(['base_uri' => 'https://firestore.googleapis.com']);
        $next_page_token = " ";
        $i = 0;
        while ($next_page_token) {
            if ($i == 0) {
                $next_page_token = "";
            }
            $response = $client->request('GET', '/v1/projects/hymns-test/databases/(default)/documents/Hymns' . $next_page_token);
            $hymns_data = json_decode($response->getBody());

            foreach ($hymns_data->documents as $hymn) {
                $hymn_insert = [
                    'title' => $hymn->fields->hymnTitle->stringValue,
                    'text' => $hymn->fields->hymnText->stringValue,
                    'number' => $hymn->fields->hymnNumber->integerValue,
                    'language' => $hymn->fields->hymnLanguage->stringValue,
                    'key' => $hymn->fields->hymnKey->stringValue,
                    'created_at' => now()
                ];
                DB::table('tbl_hymns')->insert($hymn_insert);
            }
            $next_page_token = '?pageSize=20&pageToken=' . $hymns_data->nextPageToken;
            echo 'page: ' . $i . ' <br>';
            $i++;
        }

//        loop through inserting to database.
        return response()->json(['status' => 200, 'msg' => 'complete']);


    }*/
    public function mobile_register(Request $request)
    {
        $fullname = $request->get('fullname');
        $phone = $request->get('phone');
        $country_id = $request->get('country_id');
        $password = $request->get('password');

//        generate password and send back to user.
//
        $insert_mobile_user = ['fullname' => $fullname, 'phone' => $phone, 'password' => $password, 'country_id' => $country_id, 'created_at' => Carbon::now()];
        if (DB::table('tbl_mobile_users')->insert($insert_mobile_user)) {
//            QUEUE A PIN NUMBER VIA SMS.
            return response()->json(['status' => 200, 'message' => 'Registration was successful...Use fullname and pin to login.', 'unique_id' => $password]);
        }
        return response()->json(['status' => 501, 'error' => 'Could not register user. Either the number is invalid or has been already used.']);
    }

    public function mobile_login(Request $request)
    {
        $phone = $request->get('phone');
        $unique_id = $request->get('unique_id');
        $fullname = $request->get('fullname');;

//        find phone and match at least one name...lock account as logged in.
        if ($user = DB::table('tbl_mobile_users')->where('phone', $phone)->where('password', $unique_id)->first()) {
            similar_text($fullname, $user->fullname, $percent);
            if ($percent > 49) {
                $username = $user->fullname;
                /*if ($user->logged_in == 0) {
                    $username = "-> Your hymns are downloading in a moment. Give a minute or two.";
                }*/
                DB::table('tbl_mobile_users')->where('id', $user->id)->update(['logged_in' => DB::raw('logged_in+1')]);
                return response()->json(['status' => 200, 'id' => $user->id, 'phone' => $user->phone, 'country' => $user->country_id, 'fullname' => $username]);
            } else {
                return response()->json(['status' => 501, 'error' => 'Please give your registration name.']);
            }
        } else {
            return response()->json(['status' => 501, 'error' => 'Looks like that is someone else\'s account.']);
        }
    }
}
