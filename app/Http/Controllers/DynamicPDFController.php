<?php

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use App\HymnsUser;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use PDF;
use Illuminate\Support\Facades\DB;
use DateTime as GlobalDateTime;

class DynamicPDFController extends Controller
{
    function index()
    {
        $result = DB::table('transaction_entries')->limit(10)->get();
        $data['reports'] = $result;

        return view('pdf')->with($data);
    }

    public function export(/*Request $request*/)
    {
        /*$uid = $request->get('id');
        $email = $request->get('email');
        $from = $request->get('from');
        $to = $request->get('to');*/

        $abi = new Abi();

        $facility_accounts = $abi->get_all_user_facility_accounts(23);
        $facility_accounts_list = array();
        if ($facility_accounts) {
            foreach ($facility_accounts as $ac) {
//                $facility_accounts_list[] = ;
                $facility_accounts_list[] = $ac['id'];
            }
        }
        $result = DB::table('transaction_entries')->whereIn('tx_type', [2, 3, 13, 25, 21])->whereIn('account_id', $facility_accounts_list)->get();
        if (isset($result)) {
            $count = 0;
            foreach ($result as $transaction) {
                $result[$count]->tx_name = $abi->tx_type_in_words($transaction->tx_type);
                $count++;
            }
        }
        $data['reports'] = $result;

        $pdf = PDF::loadView('pdf_file_save', $data);

        $pdf->save(public_path('/') . '/files/warui_filename2.pdf');
        unset($pdf);


        return view('pdf_export')->with($data);
    }

    private function date_and_ran()
    {
        $time = Carbon::now('Africa/Nairobi')->format('mdhi');
        try {
            return $time . '_' . random_int(1, 10);
        } catch (\Exception $e) {
        }
        return $time . '_';
    }

    public function email_bank_report(Request $request)
    {
        $uid = $request->get('user_id');
        $email = $request->get('email');
        $from = $request->get('from');
        $to = $request->get('to');

        $from = date($from);
        $to = date('Y-m-d H:i:s', strtotime($to) + 86400);
        $amount = $this->tarriffCalc($from, $to);

        $now = date('Ymd_Hm');
        $abi = new Abi();
        $user = HymnsUser::find($uid);

        $profits_balance = $abi->get_account_balance('balance_general', Config::get('constants.sys_ac.profit'));
        $bank_account_id = $abi->get_user_account($uid, Config::get('constants.ac_types.bank'));
        $bank_balance = $abi->get_account_balance('balance_bank', $bank_account_id);

        if ($bank_balance < $amount) {
            return response()->json(['msg' => 'You do not have sufficient funds to process this report']);
        }

        $result = DB::table('transaction_entries')->where('account_id', $bank_account_id)->whereIn('tx_type', [1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 14, 17, 19, 22, 32, 33, 34, 39, 41, 44, 45])->whereBetween('time_created', [$from, $to])->orderBy('time_created', 'DESC')->get();

        if (isset($result)) {
            $count = 0;
            foreach ($result as $transaction) {
                $result[$count]->tx_name = $abi->tx_type_in_words($transaction->tx_type);
                $count++;
            }
        }
        $data['reports'] = $result;
        $data['type'] = 'bank';
        $data['timeline'] = ['from' => date('jS, M Y', strtotime($from)), 'to' => date('jS, M Y', strtotime($to))];

        $file_name = $user->idpass . '_' . $this->date_and_ran() . '_cac.pdf';
        $pdf = PDF::loadView('pdf_file_save', $data);
        $pdf->save(public_path('/') . 'files/' . $file_name);
        unset($pdf);


//        queue in mail and send.
        $subject = 'Homepesa SACCO REPORTS';
        $message = 'Attached is your current account report at Homepesa SACCO for the period ' . $from . ' to ' . $to;
        $files = public_path('/') . 'files/' . $file_name;

        $sign_up_email_bundle = array('subject' => $subject, 'message' => $message, 'email' => $email, 'files' => $files);

        try {
            QueueGrails::dispatch(2, $sign_up_email_bundle);
            unset($sign_up_email_bundle);
//            charge guy 50 bob
            $tx_type = Config::get('constants.tx_types.report_charges');

            if ((int)$amount > 0) {
                $tx_id = $abi->create_transaction($amount, $tx_type, $uid);
//		dr bank
                $abi->performTransactionEntry($tx_id, $bank_account_id, Config::get('constants.ac_types.bank'), $amount, 2, $tx_type, $bank_balance, $bank_balance - $amount, $abi->now);
//		cr profits
                $abi->performTransactionEntry($tx_id, Config::get('constants.sys_ac.profit'), Config::get('constants.ac_types.general'), $amount, 1, $tx_type, $profits_balance, $profits_balance + $amount, $abi->now);
                if (!$abi->commit_transaction($tx_id)) {
                    return response()->json(['status' => 501, 'error' => 'could not complete transaction at the moment']);
                }
            }

        } catch (Exception $e) {
            dd($e);
            return $e->getCode();
        }
        if ((int)$amount > 0) {
            $abi->mark_transaction_state($tx_id, 'COMPLETE');
        }

        return response()->json(['msg' => 'Report sent to mail.']);
    }


    public function export_pdf()
    {
        // Fetch all customers from database
        $result = DB::table('transaction_entries')->limit(10)->get();
        $data['reports'] = $result;
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('pdf', $data);

        $pdf->save(public_path('/') . '/files/warui_filename.pdf');
        unset($pdf);


//        queue on email
        // Finally, you can download the file using download function
        return 'finished';
//        return $pdf->download('customers.pdf');
    }

    public function viewPDF()
    {
        $users = $this->user->get();

        $pdf = PDF::loadView('pdf.users', ['users' => $users]);

        return $pdf->setPaper('a4')->stream();
    }

    public function tarriffCalc($fromdate, $todate)
    {

        $fdate = new GlobalDateTime($fromdate);
        $tdate = new GlobalDateTime($todate);
        $interval = date_diff($fdate, $tdate);
        $days = $interval->days;

        if ($days < 0) {
            return response()->json(['msg' => 'Ensure That Dates are input correctly']);
        } elseif ($days <= 90) {
            return 0;
        } elseif ($days <= 150) {
            return 20;
        } elseif ($days > 150) {
            return 30;
        }

        return 0;
    }
}
