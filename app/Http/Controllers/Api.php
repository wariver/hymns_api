<?php
/**
 * Created by Warui.
 * User: Warui
 * Date: 8/22/18
 * Time: 5:37 PM
 */

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPMailer;
use phpmailerException;

class Api extends Controller
{
    public $link;
    public $now;

    /**
     * Api constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Africa/Nairobi');
        $this->now = date('Y-m-d H:i:s');
    }

    public function test()
    {
        return response()->json('mths');
    }

    function sendEmail($subject, $message, $recipient, $files)
    {
        $status = 0;
        try {

            $crendentials = array(
                'email' => 'auth.homepesa.sacco@gmail.com',
                'password' => 'HOMEPESAsacc0',

            );
            $smtp = array(

                'host' => 'smtp.gmail.com',
                'port' => '587',
//                'port' => '465',
                'auth' => true,
                'username' => $crendentials['email'],
                'password' => $crendentials['password'],
                'secure' => 'tls' //SSL or TLS
//                'secure' => 'ssl' //SSL or TLS

            );

            /* $crendentials = array(
                 'email' => 'info@homepesasacco.com',
                 'password' => 'investment123#',

             );
             $smtp = array(

                 'host' => 'smtp.zoho.com',
                 'port' => '587',
 //                'port' => '465',
                 'auth' => true,
                 'username' => $crendentials['email'],
                 'password' => $crendentials['password'],
                 'secure' => 'tls' //SSL or TLS
 //                'secure' => 'ssl' //SSL or TLS

             );*/
            $email = new PHPMailer();

            //SMTP Configuration
            $email->isSMTP();
            $email->isHTML(true);
            $email->SMTPAuth = true; //We need to authenticate
            $email->Host = $smtp['host'];
            $email->Port = $smtp['port'];
            $email->Username = $smtp['username'];
            $email->Password = $smtp['password'];
            $email->SMTPSecure = $smtp['secure'];

            $bodytext = $message;

            $email->From = 'auth.homepesa.sacco@gmail.com';
            $email->FromName = 'Homepesa SACCO';
            $email->Subject = $subject;
            $email->Body = $bodytext;

            $email->SMTPSecure = 'tls'; //secure transfer enabled

            $email->AddAddress($recipient);

            //add files
            $files = trim($files, ',');
            $filesVals = explode(',', $files);
            if (count($filesVals) > 0) {
                foreach ($filesVals as $tumayeye) {
                    if ($tumayeye !== '') {
                        $name = basename($tumayeye);
                        $email->AddAttachment($tumayeye, $name);
                    }
                }
            }

            $emailResult = $email->Send();
            if ($emailResult === false) {
                $status = 0;
            } else {
                $status = 1;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        }

        return $status;
    }

    public function phoneFormat($phone)
    {
        //initialize valuables
        $status = false;
        $formatedPhone = '';

        //remove white spaces
        $phone = trim($phone);
        //remove -, (, and )
        $phone = str_replace(array(' ', '-', '(', ')'), '', $phone);

        $length_phone = strlen($phone);
        //validate - all should begin with 254
        if ($length_phone >= 9 && $length_phone <= 13) {
            if (strpos($phone, '07') === 0) {
                $phone = substr_replace($phone, '254', 0, 1);
            } elseif (strpos($phone, '+254') === 0) {
                $phone = substr_replace($phone, '', 0, 1);
            } elseif (strpos($phone, '7') === 0) {
                $phone = substr_replace($phone, '254', 0, 0);
            }

            if (is_numeric($phone) && strpos($phone, '254') === 0 && strlen($phone) === 12) {
                $status = true;
                $formatedPhone = $phone;
            }
        }

        $array = array('status' => $status, 'formatedPhone' => $formatedPhone);

        return json_encode($array);
    }

    function time_now()
    {
        date_default_timezone_set('Africa/Nairobi');
        return date('Y-m-d H:i:s');
    }

    public function fire_donation(Request $request)
    {
        $user_id = (int)$request->get('user_id');
        $phone = (string)$request->get('phone');
        $amount = (int)$request->get('amount');
        $insert_donation = ['user_id' => $user_id, 'phone' => $phone, 'amount' => $amount, 'created_at' => Carbon::now()];
        if (DB::table('tbl_donation_requests')->insert($insert_donation)) {
            $client = new \GuzzleHttp\Client();
            $response = $client->post(
                'http://mpesa.cfministry.church/simulate.php',
                array(
                    'form_params' => array(
                        'cause' => '18224.18224', 'phone' => $phone, 'amount' => $amount
                    )
                )
            );
//            dd(json_decode($response->getBody()));
            return response()->json(['status' => 200, 'message' => 'MPESA Sent']);
        } else {
            return response()->json(['error' => 'Failed to insert your request please try again later.'], 500);
        }

    }
}
