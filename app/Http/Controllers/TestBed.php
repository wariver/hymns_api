<?php

namespace App\Http\Controllers;

use App\Jobs\QueueGrails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestBed extends Controller
{
    //
    public function delUser($user_id): void
    {
        DB::table('tbl_users')->where('id', $user_id)->delete();

        echo 'done';
    }

}
