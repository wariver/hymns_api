<?php

namespace App\Http\Controllers;

use App\Hymns;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AjaxController extends Controller
{
    //
    /**
     * AjaxController constructor.
     */
    public function __construct()
    {
    }

    public function ajax_hymns(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::select(
                DB::raw(
                    'select id, title, SUBSTRING(text, 1, 35) as text, number, language from tbl_hymns where deleted != 1'
                )
            );
//            $data = Hymns::select(['title', 'SUBSTRING(text, 1, 8) as text', 'number', 'language'])->get();
            return DataTables::of($data)->addColumn(
                'action',
                function ($data) {
                    $button = '<button type="button" name="edit" id="' . $data->id . '" class="edit btn btn-primary btn-sm">Edit</button>';
                    $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="' . $data->id . '" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $button;
                }
            )->rawColumns(['action'])->make(true);
        }
        return null;
    }

    public function ajax_update(Request $request)
    {
        $rules = [
            'title' => 'required',
            'text' => 'required',
            'number' => 'required',
            'language' => 'required'
        ];
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
        $form_data = [
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'number' => $request->get('number'),
            'language' => $request->get('language'),
            'updated_at' => Carbon::now('Africa/Nairobi'),
        ];
        if (Hymns::where('id', $request->get('id'))->update($form_data) === 1) {
            return response()->json(['status' => 200, 'success' => 'Successfully updated']);
        }
        return response()->json(['status' => 500, 'error' => 'There was a problem']);
    }

    public function ajax_destroy($id)
    {
        if (request()->ajax()) {
            return Hymns::where('id', $id)->update(['deleted_at' => now('Africa/Nairobi'), 'deleted' => 1]);
        }
        return null;
    }

    public function ajax_add(Request $request)
    {
        $rules = [
            'title' => 'required',
            'text' => 'required',
//            'number' => 'required',
            'language' => 'required'
        ];
        $error = Validator::make($request->all(), $rules);
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
        if (!$request->number) {
//            select the highest number for hymn
            $request->number = Hymns::max('number') + 1;
        }
        $form_data = [
            'title' => $request->title,
            'text' => $request->text,
            'number' => $request->number,
            'language' => $request->language,
            'created_at' => Carbon::now('Africa/Nairobi'),
            'updated_at' => Carbon::now('Africa/Nairobi'),
        ];

        $hymn_id = Hymns::insertGetId($form_data);
        return response()->json(['success' => 'Data added successfully.', 'hymn_id' => $hymn_id]);
    }

    public function ajax_edit($id)
    {
        if (request()->ajax()) {
            return response()->json(['result' => Hymns::find($id)]);
        }
        return null;
    }
}
