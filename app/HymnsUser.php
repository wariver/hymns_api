<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HymnsUser extends Model
{
    //
    protected $table = 'tbl_users';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'phone', 'actype', 'acstatus', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];
}
