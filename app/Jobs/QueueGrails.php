<?php

namespace App\Jobs;

use App\Http\Controllers\Api;
use App\Notifications\PasswordResetRequest;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class QueueGrails implements ShouldQueue {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	protected $job_type;
	protected $param1;
	protected $api;


    /**
     * Create a new job instance.
     *
     * @param $_job_type: Job type
     * @param $_param1: data carried to job
     */
	public function __construct( $_job_type, $_param1 ) {
		$this->job_type = $_job_type;
		$this->param1   = $_param1;
		$this->api      = new Api();
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		//
		switch ( $this->job_type ) {
//			    sms queue
			case 1:
				$phone   = $this->param1['phone'];
				$message = $this->param1['message'];
				$this->api->sendMobileSasa( $phone, $message );
				break;
//				email queue

			case 2:
				$subject = $this->param1['subject'];
				$message = $this->param1['message'];
				$email = $this->param1['email'];
				$files = $this->param1['files'];

				$this->api->sendEmail( $subject, $message, $email, $files );
				break;
//				iprs checking
			case 3:
				break;
//				giving CRB
			case 4:
				break;
//				posting to flex cube
			case 5:

				break;
            case 6:
//                $this->param1    is $user
                $this->param1->notify(new SignupActivate($this->param1));
                break;
            case 7:
                $this->param1['user']->notify(new PasswordResetRequest($this->param1['token']));
                break;
//
		}
	}
}
