<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Hymns
 *
 * @mixin Builder
 * */
class Hymns extends Model
{
    //
    protected $table = 'tbl_hymns';
}
