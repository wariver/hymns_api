<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('export', 'DynamicPDFController@export');
Route::get('pdf', ['as' => 'view-pdf_1', 'uses' => 'DynamicPDFController@index']);
Route::get('view-pdf/', ['as' => 'view-pdf', 'uses' => 'DynamicPDFController@viewPDF']);
Route::get('download-pdf/', ['as' => 'download-pdf', 'uses' => 'DynamicPDFController@export_pdf']);

Route::get('hymns', 'HymnsController@index');
?>
