<?php

use Illuminate\Support\Facades\Route;

Route::get('test', 'Api@test');
Route::post('mobile_register', 'MainController@mobile_register');
Route::post('mobile_login', 'MainController@mobile_login');
Route::group(
    ['prefix' => 'auth'],
    function () {
        Route::post('login', 'MainController@login');//✅
        Route::post('register', 'MainController@register');
        Route::get('register/activate/{token}', 'MainController@signupActivate');
        Route::group(
            ['middleware' => 'auth:api'],
            static function () {
                Route::get('logout', 'MainController@logout');
                Route::get('profile', 'MainController@profile');
                Route::get('user_hymns', 'MainController@user_hymns');
                Route::post('search_hymns', 'MainController@search_hymns');
                Route::post('update_hymn', 'MainController@update_hymn');
                Route::post('add_hymn', 'MainController@add_hymn');
            }
        );
    }
);
Route::get('validate-token', 'TestBed@validateToken')->middleware('auth:api');

Route::group(
    [
        'middleware' => 'api',
        'prefix' => 'password'
    ],
    function () {
        Route::post('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');
    }
);
Route::post('mpesa_payment', 'Api@mpesa_payment');
Route::post('fetch_hymns', 'MainController@fetch_new_hymns');
Route::post('fire_donation', 'Api@fire_donation');

Route::get('hymns/all', 'AjaxController@ajax_hymns')->name('hymns_all');
Route::post('hymns/add', 'AjaxController@ajax_add')->name('hymns_add');
Route::post('hymns/update', 'AjaxController@ajax_update')->name('hymns_update');
Route::get('hymns/edit/{id}', 'AjaxController@ajax_edit')->name('hymns_edit');
Route::get('hymns/delete/{id}', 'AjaxController@ajax_destroy')->name('hymns_delete');






