<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Welcome to Waves User Portal">
    <meta name="author" content="Homepesa">
    <title>HOMEPESA SACCO</title>
    <!-- Favicon -->
    <link href="{{url('/')}}/img/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->

    <!-- Homepesa CSS -->

    <link type="text/css" href="{{ url('/') }}/css/kabooty/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="{{ url('/') }}/css/mobi-chapaa.css" rel="stylesheet">

</head>

<body style="font-family: Zetta Sans,sans-serif;">
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand pt-0" href="{{ url('/') }}">
                <img src="{{url('/')}}/img/hpesa.png" class="navbar-brand-img col-6" alt="..."
                     style="max-width: 250px;">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(isset($info))
                        <div class="card-body">
                            <span class="text-success mb-3"> {{$info}} </span>
                            <div class="row">
                                <a class="btn btn-success"
                                   href="https://homepesasacco.com/app/login">{{ __('Login') }}</a>
                            </div>
                        </div>
                    @else
                        <div class="card-body">
                            <span class="text-success mb-3"> {{$error}} </span>
                            <div class="row">
                                <a class="btn btn-success"
                                   href="https://homepesasacco.com/">{{ __('Home') }}</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Homepesa Scripts -->
<!-- Core -->
@include('js_template')
</body>

</html>
