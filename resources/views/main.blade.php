@extends('layouts.app')
@section('style-scripts')
@endsection
@section('content')
    <div class="main-content">

        <!-- Header -->
        <div class="header bg-dark pb-4 pt-2 pt-md-4">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-stats mb-4 mb-xl-0">
                                <div class="card-title">
                                    <h3 class="text-center text-white">Hymns Table</h3>
                                </div>
                                <div class="card-body">
                                    <div class="container">
                                        <div align="right">
                                            <button type="button" name="create_record" id="create_record"
                                                    class="btn btn-success">Add New Hymn
                                            </button>
                                        </div>
                                        <br/>
                                        <div class="table-responsive">
                                            <table id="hymns_table" class="table table-bordered table-light">
                                                <thead>
                                                <th width="35%">Title</th>
                                                <th width="35%">Text</th>
                                                <th width="35%">Number</th>
                                                <th width="35%">Language</th>
                                                <th width="30%">Action</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="formModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg modal-secondary">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Add Hymn</h4>
                                                    <button type="button" class="close" aria-label="Close"><span
                                                                class="text-darker" aria-hidden="true">&times;</span>
                                                    </button>
                                                    <br>
                                                </div>
                                                <div class="modal-body">
                                                    <span id="form_result"></span>
                                                    <form method="post" id="hymns-form" class="form-horizontal">
                                                        <div class="tab-content">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label"
                                                                               for="title">Title</label>
                                                                        <input type="text" id="title" name="title"
                                                                               class="form-control form-control-alternative"
                                                                               placeholder="Hymn Title"
                                                                               value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label" for="number">Number</label>
                                                                        <input type="text" id="number"
                                                                               name="number"
                                                                               class="form-control form-control-alternative"
                                                                               value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label" for="text">Text</label>
                                                                        <textarea type="text" id="text" name="text"
                                                                                  class="form-control form-control-alternative"
                                                                                  rows="15">

                                                                        </textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-8">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-8">Language</label>
                                                                        <select class="form-control" name="language">
                                                                            <option value="English">English</option>
                                                                            <option value="Kiswahili">Kiswahili</option>
                                                                            <option value="Kikuyu">Kikuyu</option>
                                                                            <option value="Kamba">Kamba</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" align="center">
                                                                <input type="submit" name="action_button"
                                                                       id="action_button"
                                                                       class="btn btn-warning" value="Add Hymn"/>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- EDIT STUDENT RECORD MODAL --}}
                                    <div id="editModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-lg modal-secondary">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="edit_title"></h4>
                                                    <button type="button" class="close" aria-label="Close"><span
                                                                class="text-darker" aria-hidden="true">&times;</span>
                                                    </button>
                                                    <br>
                                                </div>
                                                <div class="modal-body">
                                                    <span id="edit_result"></span>
                                                    <form method="post" id="edit-hymns-form" class="form-horizontal">
                                                        <div class="tab-content">
                                                            @csrf
                                                            <input name="id" value="id" hidden>
                                                            <div class="row">
                                                                <div class="col-lg-8">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label"
                                                                               for="title">Title</label>
                                                                        <input type="text" id="title" name="title"
                                                                               class="form-control form-control-alternative">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label" for="number">Number</label>
                                                                        <input type="text" id="number"
                                                                               name="number"
                                                                               class="form-control form-control-alternative"
                                                                               value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-8">Language</label>
                                                                        <select class="form-control" name="language">
                                                                            <option value="English">English</option>
                                                                            <option value="Kiswahili">Kiswahili</option>
                                                                            <option value="Kikuyu">Kikuyu</option>
                                                                            <option value="Kamba">Kamba</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="form-control-label"
                                                                               for="student_name">Text</label>
                                                                        <textarea type="text" id="text" name="text"
                                                                                  class="form-control-plaintext"
                                                                                  rows="12"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" align="center">
                                                                <input type="submit" name="action_button"
                                                                       id="action_button"
                                                                       class="btn btn-warning"
                                                                       value="Edit Hymn"/>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="confirmModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                    <h2 class="modal-title">Confirmation</h2>
                                                </div>
                                                <div class="modal-body">
                                                    <h4 align="center" style="margin:0;">Are you sure you want to remove
                                                        this data?</h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" name="ok_button" id="ok_button"
                                                            class="btn btn-danger">OK
                                                    </button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    {{--                    SMTH DOWN HERE--}}

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-scripts')
    <script>
        function feedback(response, status) {

            VanillaToasts.create({
                title: status,
                text: response,
                type: status, // success, info, warning, error   / optional parameter
                // icon: '/img/alert-icon.jpg', // optional parameter
                positionClass: 'topRight',
                timeout: 7000 // hide after 5000ms, // optional parameter
                // callback: function() { ... } // executed when toast is clicked / optional parameter
            });
        }
    </script>

    <script>

        $(document).ready(function () {
            $('#hymns_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('hymns_all') }}",
                },
                columns: [
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'text',
                        name: 'text'
                    },
                    {
                        data: 'number',
                        name: 'number'
                    },
                    {
                        data: 'language',
                        name: 'language'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ]
            });
            $('#create_record').click(function () {
                $('.modal-title').text('Add New Record');
                $('#action_button').val('Add');
                $('#action').val('Add');
                $('#form_result').html('');
                console.log();
                $('#formModal').modal('show');
            });
            var hymnsForm = $('#hymns-form');
            var editHymnsForm = $('#edit-hymns-form');
            hymnsForm.on('submit', function (event) {
                event.preventDefault();
                var createForm = hymnsForm.closest('form');
                var record = new FormData(createForm[0]);
                var action_url = '{{ route('hymns_add') }}';

                $.ajax({
                    url: action_url,
                    method: "POST",
                    data: record,
                    // dataType: "json",
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        var html = '';
                        if (data.errors) {
                            html = '<div class="alert alert-danger">';
                            for (var count = 0; count < data.errors.length; count++) {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';
                        }
                        if (data.success) {
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            hymnsForm[0].reset();
                            $('#hymns_table').DataTable().ajax.reload();
                        }
                        $('#form_result').html(html);
                    }
                });
            });

            editHymnsForm.on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('hymns_update')}}",
                    method: "POST",
                    data: $(this).serialize(),
                    dataType: "json",
                    success: function (data) {
                        var html = '';
                        console.log(data);
                        if (data.status === 500) {
                            feedback(data.errors, 'warning');
                            /*html = '<div class="alert alert-danger">';
                            for (var count = 0; count < data.errors.length; count++) {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';*/
                        }
                        if (data.status === 200) {
                            feedback(data.success, 'success');
                            /*html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#hymns_table').DataTable().ajax.reload();
                            editHymnsForm[0].reset();*/
                        }
                        $('#edit_result').html(html);
                    }
                });
            });
            $(document).on('click', '.edit', function () {
                $('#edit_result').html("");
                var id = $(this).attr('id');

                $('#form_result').html('');
                var editModal = $('#editModal');
                $.ajax({
                    url: "api/hymns/edit/" + id,
                    dataType: "json",
                    success: function (data) {
                        editModal.find('[name="id"]').val(data.result.id);
                        editModal.find('[name="title"]').val(data.result.title);
                        editModal.find('[name="text"]').val(data.result.text);
                        editModal.find('[name="number"]').val(data.result.number);
                        editModal.find('[name="language"]').val(data.result.language);
                        $('#edit_title').text('Edit Song: ' + data.result.title);

                        editModal.modal('show');
                    }
                })
            });
            var user_id;
            var confirmModal = $('#confirmModal');
            $(document).on('click', '.delete', function () {
                user_id = $(this).attr('id');
                confirmModal.modal('show');
            });

            $('#ok_button').click(function () {
                $.ajax({
                    url: "api/hymns/delete/" + user_id,
                    beforeSend: function () {
                        $('#ok_button').text('Deleting...');
                    },
                    success: function (data) {
                        setTimeout(function () {
                            confirmModal.modal('hide');
                            $('#hymns_table').DataTable().ajax.reload();
                            alert('Data Deleted');
                        }, 2000);
                    }
                })
            });

        });
    </script>
@endsection
