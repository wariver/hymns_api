<script src="{{ url('/') }}/vendor/jquery/dist/jquery.min.js"></script>
<script src="{{ url('/') }}/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
{{--<script src="{{ url('/') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>--}}
<!-- Optional JS -->
<script src="{{ url('/') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ url('/') }}/vendor/chart.js/dist/Chart.extension.js"></script>
<!-- dib_waves JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-waitingfor/1.2.7/bootstrap-waitingfor.min.js"></script>
<script src="{{ url('/') }}/js/mobi_chapaa.js"></script>
<script>
    // var base_url = 'http://localhost/waves_api/public';
    var base_url = 'https://homepesasacco.com/edx/pg_homepesa_api/public';
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
