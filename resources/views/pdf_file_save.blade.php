<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your journey to financial freedom with Homepesa SACCO.">
    <meta name="author" content="Homepesa SACCO">
    <title>Homepesa SACCO</title>
    <!-- Favicon -->
    <link href="https://homepesasacco.com/img/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    {{--    @include('css_template')--}}
    <link type="text/css" href="{{ url('/') }}/css/mobi_chapaa.css" rel="stylesheet">
</head>

<body style="font-family: 'Zetta Sans';">
<!-- Sidenav -->
{{--@include('side_nav')--}}
<!-- Main content -->
<div class="main-content">
    <!-- Top navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
        </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-6">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->
                <!-- Header container -->
                <div class="container-fluid d-flex align-items-center">
                    <div class="row">
                        <div class="col-lg-11 col-md-11">
                            {{--                            <h1 class="display-2 text-white">LOAN REPORT</h1>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--100">
        <h3 style="margin-bottom: 8px; font-family: 'Zetta Sans';"> Homepesa SACCO</h3>
        <span style="float: right;margin-bottom: 8px;">{{date('jS, M Y g:i a')}}</span>

        <div class="row">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="table-responsive">
                            @if(isset($type))
                                @if($type === 'loans')
                                    <h3 style="margin-bottom: 4px;"> Loan Reports </h3>
                                @elseif($type === 'savings')
                                    <h3 style="margin-bottom: 4px;"> Savings Reports </h3>
                                @elseif($type === 'bank')
                                    <h3 style="margin-bottom: 4px;"> Current Account Reports </h3>
                                @elseif($type === 'tx')
                                    <h3 style="margin-bottom: 4px;"> Transactions Report</h3>
                                @endif

                            @endif

                            <h4 style="margin-bottom: 8px;">Period: {{$timeline['from']}} to {{$timeline['to']}}</h4>

                            <table class="table align-items-center table-flush">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Time</th>
                                    <th>Narrative</th>
                                    <th>Paid</th>
                                    <th>Received</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $count = 0;@endphp
                                @foreach($reports as $transaction)
                                    <tr>
                                        <td>{{$count}}</td>
                                        <td>{{date('jS, M Y g:i a',  strtotime($transaction->time_created))}}</td>
                                        <td>{{$transaction->tx_name}}</td>
                                        @if($transaction->debit_credit_flag === 1)
                                            <td>
                                                {{$transaction->amount}}
                                            </td>
                                            <td></td>
                                        @else
                                            <td></td>
                                            <td>
                                                {{$transaction->amount}}
                                            </td>

                                        @endif
                                        <td>{{$transaction->balance_after}}</td>
                                    </tr>
                                    @php $count++; @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="row mt-5">
                </div>


                <!-- Footer -->
                <footer class="footer">
                    <span style="text-align: center">&copy; 2019 <a href="{{ url('/') }}/" class="font-weight-bold ml-1"
                                                                    target="_blank">Homepesa SACCO</a></span>
                </footer>
            </div>
        </div>
    </div>
</div>
<!-- dib_waves Scripts -->
@include('js_template')
</body>

</html>
