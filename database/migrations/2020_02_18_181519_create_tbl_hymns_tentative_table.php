<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHymnsTentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hymns_tentative', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150)->nullable();
            $table->integer('user_id')->nullable();
            $table->text('text');
            $table->integer('number')->nullable();
            $table->string('key', 2)->nullable();
            $table->string('language', 20)->nullable();
            $table->smallInteger('approved')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hymns_tentative');
    }
}
