<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUpdateTableFx extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE OR REPLACE FUNCTION fn_updatedat() RETURNS TRIGGER AS $$ BEGIN NEW.updated_at = NOW(); RETURN NEW; END; $$ language \'plpgsql\';');
        DB::unprepared('CREATE TRIGGER trg_tbl_hymns_updatedat BEFORE UPDATE ON tbl_hymns FOR EACH ROW EXECUTE PROCEDURE fn_updatedat();');
        DB::unprepared('CREATE TRIGGER trg_tbl_mobile_updatedat BEFORE UPDATE ON tbl_mobile_users FOR EACH ROW EXECUTE PROCEDURE fn_updatedat();');
        DB::unprepared('CREATE TRIGGER trg_tbl_users_updatedat BEFORE UPDATE ON tbl_users FOR EACH ROW EXECUTE PROCEDURE fn_updatedat();');

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS trg_tbl_hymns_updatedat ON tbl_hymns CASCADE;');
        DB::unprepared('DROP TRIGGER IF EXISTS trg_tbl_mobile_updatedat ON tbl_mobile CASCADE;');
        DB::unprepared('DROP TRIGGER IF EXISTS trg_tbl_users_updatedat ON tbl_users CASCADE;');
        DB::unprepared('DROP PROCEDURE IF EXISTS fn_updatedat();');
    }
}
