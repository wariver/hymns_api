<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMobileUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mobile_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 100)->nullable();
            $table->string('phone', 15)->unique()->nullable();
            $table->string('password', 100)->nullable();
            $table->integer('country_id')->nullable();
            $table->smallInteger('logged_in')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mobile_users');
    }
}
