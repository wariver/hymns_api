<?php
/**
 * Created by PhpStorm.
 * User: Warui
 * Date: 2/16/2019
 * Time: 12:55 PM
 */
return [
    'ac_types' => [
        'bank' => 1,
        'savings' => 2,
        'holding' => 3,
        'facility' => 4,
        'dcf' => 5,
        'school_fees' => 6,
        'general' => 7,
        'memberships_business' => 12,
        'memberships_business_holding' => 13,
        'memberships_group' => 10,
        'memberships_holding' => 11,
        'memberships_sacco' => 14,
        'memberships_sacco_holding' => 15,
        'county_parking' => 20,
        'county_b_permits' => 22,
        'county_l_rates' => 21,
        'stores' => 30,
    ],
    'tx_types' => [
        'send_to_member' => 1,
        'facility_disbursement' => 2,
        'bank_repayment' => 3,
        'bank_savings' => 4,
        'savings_bank' => 5,
        'savings_holding' => 6,
        'holding_savings' => 7,
        'c2b_bank' => 9,
        'bank_dcf' => 10,
        'bank_airtime' => 14,
        'c2b_repayment' => 13,
        'c2b_savings' => 15,
        'c2b_dcf' => 16,
        'asset_profit_upfront' => 20,
        'facility_creation' => 21,
        'offset_facility' => 25,
        'c2b_group_savings' => 30,
        'group_member' => 31,
        'pay_bills' => 34,
        'c2b_sacco' => 36,
        'group_facility_disbursement' => 37,
        'sacco_facility_disbursement' => 38,
        'bank_parking' => 39,
        'c2b_parking' => 40,
        'bank_b_permit' => 41,
        'c2b_b_permit' => 42,
        'c2b_l_rate' => 43,
        'bank_l_rate' => 44,
        'report_charges' => 45,
        'surplus_repayment' => 46,
        'card_bank' => 47,
        'card_savings' => 48

    ],
    'sys_ac' => [
        'bank_asset' => 1,
        'c2b' => 2,
        'b2c' => 3,
        'profit' => 4,
        'bank_facility_account' => 5,
        'current_account' => 6,
        'facility_repayments' => 7,
        'transaction_charges' => 8,
        'homepesa_card' => 12
    ],
    'cr' => 1,
    'dr' => 2
];
